package ru.volkova.tm.service;

import ru.volkova.tm.api.repository.IProjectRepository;
import ru.volkova.tm.api.service.IProjectService;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.model.Project;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        if (comparator == null) return null;
        return projectRepository.findAll(comparator);
    }

    @Override
    public Project add(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        return project;
    }

    @Override
    public void add(final Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project findOneById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findOneById(id);
    }

    @Override
    public Project removeOneById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.removeOneById(id);
    }

    @Override
    public Project findOneByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findOneByName(name);
    }

    @Override
    public Project removeOneByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeOneByName(name);
    }

    @Override
    public Project removeOneByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.removeOneByIndex(index);
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project updateProjectByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        project .setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project = findOneById(id);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startProjectById(String id) {
        if (id == null || id.isEmpty()) return null;
        final Project project = findOneById(id);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setDateStart(new Date());
        return project;
    }

    @Override
    public Project startProjectByName(String name) {
        if (name == null || name.isEmpty()) return null;
        final Project project = findOneByName(name);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setDateStart(new Date());
        return project;
    }

    @Override
    public Project startProjectByIndex(Integer index) {
        if (index == null || index < 0) return null;
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setDateStart(new Date());
        return project;
    }

    @Override
    public Project finishProjectById(String id) {
        if (id == null || id.isEmpty()) return null;
        final Project project = findOneById(id);
        if (project == null) return null;
        project.setStatus(Status.COMPLETE);
        project.setDateFinish(new Date());
        return project;
    }

    @Override
    public Project finishProjectByName(String name) {
        if (name == null || name.isEmpty()) return null;
        final Project project = findOneByName(name);
        if (project == null) return null;
        project.setStatus(Status.COMPLETE);
        project.setDateFinish(new Date());
        return project;
    }

    @Override
    public Project finishProjectByIndex(Integer index) {
        if (index == null || index < 0) return null;
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        project.setStatus(Status.COMPLETE);
        project.setDateFinish(new Date());
        return project;
    }

    @Override
    public Project changeProjectStatusById(String id, Status status) {
        if (id == null || id.isEmpty()) return null;
        final Project project = findOneById(id);
        if (project == null) return null;
        project.setStatus(status);
        if (status == Status.IN_PROGRESS) project.setDateStart(new Date());
        if (status == Status.COMPLETE) project.setDateFinish(new Date());
        return project;
    }

    @Override
    public Project changeProjectStatusByName(String name, Status status) {
        if (name == null || name.isEmpty()) return null;
        final Project project = findOneByName(name);
        if (project == null) return null;
        project.setStatus(status);
        if (status == Status.IN_PROGRESS) project.setDateStart(new Date());
        if (status == Status.COMPLETE) project.setDateFinish(new Date());
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(Integer index, Status status) {
        if (index == null || index < 0) return null;
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        project.setStatus(status);
        if (status == Status.IN_PROGRESS) project.setDateStart(new Date());
        if (status == Status.COMPLETE) project.setDateFinish(new Date());
        return project;
    }

}
